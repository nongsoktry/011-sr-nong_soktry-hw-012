import React, { Component } from "react";
import Cards from "../Pages/Cards";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postCards: [
        {
          id: 1,
          title: "What is HTML?",
          description:
            "Hypertext Markup Language is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets and scripting languages such as JavaScript.",
          img:
            "https://3.bp.blogspot.com/-7x5c_f1yzsQ/XHv9FZKHrEI/AAAAAAAADrE/4iGl9Lm6K2odX4SdWbU_RN6gZesx4IaGACEwYBhgL/s1600/html.jpg",
        },
        {
          id: 2,
          title: "What is CSS?",
          description:
            "Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language like HTML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.",
          img:
            "https://colorlib.com/wp/wp-content/uploads/sites/2/creative-css3-tutorials.jpg",
        },
        {
          id: 3,
          title: "What is JavaScript?",
          description:
            "JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. It has curly-bracket syntax, dynamic typing.",
          img:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQVurjGND2XQDdMAo9De51HufnG91bqEIjA0V0EyBjmVfM_NG-d&usqp=CAU",
        },
        {
          id: 4,
          title: "What is ReactJS?",
          description:
            "React is an open-source JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.",
          img: "https://miro.medium.com/max/2560/1*WfhViON2evomOK2tw09AuQ.jpeg",
        },
        {
          id: 5,
          title: "The Component Lifecycle",
          description:
            "Each component has several “lifecycle methods” that you can override to run code at particular times in the process. You can use this lifecycle diagram as a cheat sheet. In the list below, commonly used lifecycle methods are marked as bold.",
          img:
            "https://softwareengineeringdaily.com/wp-content/uploads/2018/04/React_graphic.png",
        },
        {
          id: 6,
          title: "What and Why ReactJS!",
          description:
            "React allows developers to create large web applications that can change data, without reloading the page. The main purpose of React is to be fast, scalable, and simple. It works only on user interfaces in the application.",
          img:
            "https://ares.decipherzone.com/blog-manager/uploads/banner_webp_da06d145-93f9-4df9-8c7e-1e2c332c3a4a.webp",
        },
      ],
    };
  }

  render() {
    let postCards = this.state.postCards.map((postCard) => (
      <Cards key={postCard.id} data={postCard} />
    ));
    return <div className="row">{postCards}</div>;
  }
}

export default Home;
