import React from "react";
import { Link, Route } from "react-router-dom";

const SelectCategory = ({ match, data, onClick }) => {
  let category = match.params.id;
  let list = data.find(({ id }) => id === category);

  const Chosen = ({ match }) => {
    return <h4>This is {match.params.chosenId}!</h4>;
  };

  return (
    <div>
      <h1>{list.type} Category</h1>
      <ul>
        {list.list.map((item) => (
          <li key={item.id} onClick={onClick}>
            <Link to={`/video/${category}/${item.id}`}>{item.type}</Link>
          </li>
        ))}
      </ul>

      <Route path="/video/:id/:chosenId" component={Chosen} />
    </div>
  );
};

export default SelectCategory;