import React, { Component } from "react";
import { Link, Route } from "react-router-dom";
import SelectCategory from "./SelectCategory";

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      genre: [
        {
          id: "animation",
          type: "Animation",
          list: [
            {
              id: "action",
              type: "Action",
            },
            {
              id: "comedy",
              type: "Comedy",
            },
            {
              id: "romance",
              type: "Romance",
            },
          ],
        },
        {
          id: "movie",
          type: "Movie",
          list: [
            {
              id: "adventure",
              type: "Adventure",
            },
            {
              id: "comedy",
              type: "Comedy",
            },
            {
              id: "crime",
              type: "Crime",
            },
            {
              id: "documentary",
              type: "Documentary",
            },
          ],
        },
      ],
      msg: <h4>Please Select a Topic!</h4>,
    };
  }

  handleClick = () => {
    this.setState({
      msg: "",
    });
  };
  render() {
    return (
      <div>
        <h1>Select a Video Category!</h1>
        <ul>
          {this.state.genre.map((item) => (
            <li
              key={item.id}
              onClick={() =>
                this.setState({ msg: <h4>Please Select a Topic!</h4> })
              }
            >
              <Link to={`/video/${item.id}`}>{item.type}</Link>
            </li>
          ))}
        </ul>

        <Route
          path="/video/:id"
          render={(props) => (
            <SelectCategory
              {...props}
              data={this.state.genre}
              onClick={this.handleClick}
            />
          )}
        />
        {this.state.msg}
      </div>
    );
  }
}

export default Video;