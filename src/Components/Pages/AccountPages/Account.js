import React, { Component } from "react";
import { Link } from "react-router-dom";
import SelectAccount from "./SelectedAccount";

class Account extends Component {
  constructor(props) {
    super(props);

    this.state = {
      accounts: [
        {
          id: "netflix",
          name: "Netflix",
        },
        {
          id: "zillow group",
          name: "Zillow Group",
        },
        {
          id: "yahoo",
          name: "Yahoo",
        },
        {
          id: "modus create",
          name: "Modus Create",
        },
      ],
    };
  }

  render() {
    return (
      <div>
        <h2>Select an Account!</h2>
        <ul>
          {this.state.accounts.map((item) => (
            <li key={item.id} onClick={this.handleClick}>
              <Link to={`/account?name=${item.id}`}>{item.name}</Link>
            </li>
          ))}
        </ul>
        <SelectAccount val={this.props.location.search} />
      </div>
    );
  }
}

export default Account;