import React from "react";
import { Redirect } from "react-router-dom";

const Welcome = (props) => {
  return props.auth ? <h2>This is the Welcome Page after submission!</h2> : <Redirect to="/auth" />;
};

export default Welcome;
